<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>php-pear in php5 contains CWE-502 (Deserialization of Untrusted Data)
and CWE-915 (Improperly Controlled Modification of
Dynamically-Determined Object Attributes) vulnerabilities in its
Archive_Tar class. When extract is called without a specific prefix
path, can trigger unserialization by crafting a tar file with
`phar://[path_to_malicious_phar_file]` as path. Object injection can
be used to trigger destruct in the loaded PHP classes, all with
possible remote code execution that can result in files being deleted
or possibly modified.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
5.6.39+dfsg-0+deb8u2.</p>

<p>We recommend that you upgrade your php5 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1674.data"
# $Id: $
