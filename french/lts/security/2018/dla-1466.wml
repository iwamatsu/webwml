#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une augmentation de droits ou un déni de service.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5390">CVE-2018-5390</a>

<p>(SegmentSmack)</p>

<p>Juha-Matti Tilli a découvert qu'un attaquant distant peut déclencher les
pires chemins de code pour le réassemblement d'un flux TCP avec un faible
débit de paquets contrefaits pour l'occasion, menant à un déni de service
distant.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5391">CVE-2018-5391</a>

<p>(FragmentSmack)</p>

<p>Juha-Matti Tilli a découvert un défaut dans la manière dont le noyau
Linux gérait le réassemblage de paquets IPv4 et IPv6 fragmentés. Un
attaquant distant peut tirer avantage de ce défaut pour déclencher des
algorithmes de réassemblage de fragments coûteux en temps et en calcul en
envoyant des paquets contrefaits pour l'occasion, menant à un déni de
service distant.</p>

<p>Ce défaut est atténué en réduisant les limites par défaut d'utilisation
de la mémoire pour des paquets fragmentés incomplets. La même atténuation
peut être obtenue sans nécessiter de redémarrage, en réglant sysctls :</p>

<p>net.ipv4.ipfrag_high_thresh = 262144<br>
net.ipv6.ip6frag_high_thresh = 262144<br>
net.ipv4.ipfrag_low_thresh = 196608<br>
net.ipv6.ip6frag_low_thresh = 196608</p>

<p>Les valeurs par défaut peuvent encore être augmentées par une
configuration locale si nécessaire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-13405">CVE-2018-13405</a>

<p>Jann Horn a découvert que la fonction inode_init_owner de fs/inode.c
dans le noyau Linux permet à des utilisateurs locaux de créer des fichiers
avec la propriété d'un groupe non prévu permettant à des attaquants
d'augmenter leurs droits en rendant un simple fichier exécutable et SGID.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 4.9.110-3+deb9u2~deb8u1. Cette mise à jour inclut des correctifs pour
plusieurs régressions dans la dernière version mineure.</p>

<p>La version 4.9.110-3+deb9u1~deb8u1 précédente incluait tous les correctifs
ci-dessus, excepté pour
<a href="https://security-tracker.debian.org/tracker/CVE-2018-5391">CVE-2018-5391</a>,
qui doit être mitigé comme expliqué ci-dessus.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux-4.9.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1466.data"
# $Id: $
