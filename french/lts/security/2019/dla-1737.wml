#use wml::debian::translation-check translation="47c381ba60326c1ba7425175241c66bb3634f1cb" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité a été découverte dans le serveur faisant autorité,
PowerDNS, dans les versions avant 4.0.7 et avant 4.1.7. Une validation
insuffisante des données provenant de l’utilisateur lors de la construction
d’une requête HTTP issue d’une requête DNS dans le connecteur HTTP du dorsal
distant, permettait à un utilisateur distant de provoquer un déni de service en
faisant que le serveur se connecte à un nœud non valable, ou, éventuellement,
une divulgation d'informations en faisant que le serveur se connecte à un nœud
interne et extrayant de quelque façon des informations significatives à propos
de la réponse.</p>

<p>Seules les installations utilisant le paquet pdns-backend-remote sont
 touchées.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 3.4.1-4+deb8u9.</p>
<p>Nous vous recommandons de mettre à jour vos paquets pdns.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1737.data"
# $Id: $
